var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task( 'js', function(){

  gulp.src('assets/js/*.js')
      .pipe(concat('main.js'))
      .pipe(gulp.dest('js'));

  // gulp.src([
  //     'timers.js',
  //     'storage.js'
  //   ])
  //     .pipe(gulp.dest('js'));

});

gulp.task( 'css', function(){

  gulp.src('assets/sass/*.scss')
      .pipe(sass())
      .pipe(gulp.dest('css'));

});

gulp.task( 'watch', function(){
  gulp.watch('assets/js/*.js', {}, ['js']);
});
