$(document).ready(function(){

  $('#load-ajax').click(function(){

    // $('#ajax-target').load('snippet.php');

    var name = $('#name').val();

    $.get('snippet.php', {name: name}, function(data) {
      console.log(data);
      // put something here to append to a <ul>
      $('#my-ul').append(data);
    });
    // console.log('after get');

  })


  //validateForm();
  //displayMessage();
  //deleteMessage();
  //runTimer();

});

function rawAjax() {
  xml = new XMLHttpRequest();
  xml.open( 'GET', 'http://localhost/saitjs/practice/app/snippet.php', true);
  xml.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      // do something with xml.responseText
    }
  }
  // xml.send();
}

function displayMessage() {
  var message = retrieveSomething('message');
  if( message != undefined ) {
    $('body').append('<h3>'+ message +'</h3>');
  }
}

function deleteMessage() {
  $('#delete-something').click(function(){
    localStorage.removeItem('message');
    location.reload();
  });
}
