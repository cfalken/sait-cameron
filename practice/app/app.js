$(document).ready(function(){

  getTweets();
  submitTweet();
  deleteTweet();
  detectDateInput();
  // triggerDialog();

});

function triggerDialog() {
  // $dialog = $('<div id="dialog" title="Basic dialog"><p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the \'x\' icon.</p></div>');
  $('#trigger-dialog').click(function() {
    $('#dialog').dialog();
  });
}

function detectDateInput() {
  if( Modernizr.inputtypes['date'] ) {
    console.log('Supports date');
  } else {
    console.log('Does not support date');
    $('input[type=date]').datepicker();
  }
}

function deleteTweet() {
    console.log($('#tweets li button'));
  $('#tweets').on('click', 'li button', function(){
    var $button = $(this);
    var $li = $button.parent();

    $li.slideUp(1000, function(){
      $li.remove();
      var data = {
        action: 'delete',
        id: $li.attr('id')
      }
      $.post('tweets/index.php', data);
    });
  });
}

function submitTweet() {
  $('form').submit(function(event){
    event.preventDefault();
    var data = {
      action: 'save',
      tweet: $('#tweet').val()
    }
    $.getJSON('tweets/index.php', data, function(response){

      var tweetHtml =
        '<li id="'+response.id+'">'+
          data.tweet+
          '<button>delete</button>'+
        '</li>';
      $('#tweets').append(tweetHtml);

      // bad way
      // $('#tweets').empty();
      // getTweets();
    });
  });
}

function getTweets() {
  $.getJSON('tweets/index.php?action=get', function(response) {

    // only do this if you are not using $.getJSON
    // var tweets = JSON.parse(response);
    // console.log(response);
    for( var i=0; i<response.length; i++ ) {
        var object = response[i];
        // console.log(object);
        var tweetHtml =
          '<li id="'+object.Id+'">'+
            object.tweet+
            '<button>delete</button>'+
          '</li>';
        $('#tweets').append(tweetHtml);
    }
  });
}
