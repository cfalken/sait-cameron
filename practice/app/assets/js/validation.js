function validateForm() {
  var $form = $('form');

  $form.submit(function(event){
    var validDave = validateDave($form);

    if( !validDave ) {
      alert("you must be Dave");
      event.preventDefault();
    } else {
      alert("hi Dave");
      storeSomething('message', 'undefined');
    }
  });
}

function validateDave($form) {
  var $inputs = $form.find('.must-be-dave');

  var allGood = true;

  $inputs.each(function(i, element){
    var $element = $(element);
    // if( $input.val() == 'Dave' ) {
    if( !$element.val().match(/Dave/) ) {
      $element.after('<span class="help-block">You must be Dave</span>');
      allGood = false;
    }
  });

  return allGood;

}
