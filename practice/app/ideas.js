$(document).ready(function() {

  displayIdeas();
  submitIdeas();
  deleteIdeas();

});

function deleteIdeas() {
  $('#ideas').on('click', 'button', function(){
    var $button = $(this);
    var $li = $button.parent();
    var data = {
      action: 'delete',
      id: $li.attr('id')
    };
    $.post('ideas/index.php', data, function(response){
      $li.remove();
      console.log('delete response', response);
    });
  });
}

function submitIdeas() {
  $('form').submit(function(){
    var data = {
      action: 'save',
      idea: $('#idea').val()
    }
    $.post('ideas/index.php', data, function(response){
      console.log('save response', response);
      $('#ideas').append('<li>'+data.idea+'<button>delete</button></li>');
    });
  })
}

function displayIdeas() {
  var data = {
    action: 'get'
  };

  $.getJSON('ideas/index.php', data, function(response) {
    for(var i=0; i<response.length; i++) {
      $('#ideas').append('<li id="'+response[i].Id+'">'+response[i].idea+'<button>delete</button></li>');
    }
    console.log(response);
  });
}

function demo() {

  $(selector).load(url);

  $.get(url, data, function(response){

  });

  $.getJSON(url, data, function(data){

  });

  $.post(url, data, function(response){

  });

  $.ajax({
    method: 'GET',
    url: url,
    success: function() {},
    data: data,
    dataType: 'json'
  });

  console.log(response);

}
