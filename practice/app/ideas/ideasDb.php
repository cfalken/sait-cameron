<?php
/**
 * This file contains a basics ideas API
 * providing functions to create, retrieve and delete ideas,
 * as well as set up the underlying table.
 *
 * Be sure to either copy 'ideas.sqlite' or
 * create a new file of that name in the same directory as this file.
 *
 * @author Cameron Falkenhagen
 * @date	 2015-05-05
 */
/**
 * Get the database handle
 */
function getDb()
{
	$databaseName = 'ideas.sqlite';
	try {
	  $db = new PDO('sqlite:'.$databaseName);
	  $db->setAttribute(
	    PDO::ATTR_ERRMODE,
	    PDO::ERRMODE_EXCEPTION
	  );
		return $db;
	}
	catch(PDOException $e)
	{
	  print 'Exception : '.$e->getMessage();
	}
}
function dropDatabase()
{
	$db = getDb();
	$db->exec("DROP TABLE ideas");
}
/**
 * Setup up the database by
 * - creating a ideas table
 * - adding some default data
 */
function setupDatabase()
{
	$db = getDb();
	try
	{
	  //create the database
	  $db->exec("CREATE TABLE ideas (Id INTEGER PRIMARY KEY, idea TEXT)");
	  //insert some data...
		$date = date('Y-m-d');
		saveIdea('Go Vote!');
		saveIdea('#voteorelse');
		saveIdea('Be happy');
	}
	catch(PDOException $e)
	{
	  print 'Exception : '.$e->getMessage();
	}
}
function saveIdea($idea)
{
	try
	{
		// getDb()->exec("INSERT INTO ideas (idea, complete, due_date) VALUES ('$idea', $complete, '$due_date');");
		$db = getDb();
		$stmt = $db->prepare("INSERT INTO ideas (idea) VALUES (?);");
		$stmt->execute([$idea]);
		return $db->lastInsertId();
	}
	catch(PDOException $e)
	{
		print 'Exception : ' . $e->getMessage();
	}
}
function deleteIdea($id)
{
	try
	{
		getDb()->exec("DELETE FROM ideas WHERE Id=$id;");
	}
	catch(PDOException $e)
	{
		print 'Exception : ' . $e->getMessage();
	}
}
function getIdeas()
{
	try
	{
	  $result = getDb()->query('SELECT * FROM ideas');
	}
	catch(PDOException $e)
	{
		print 'Exception : '.$e->getMessage();
	}
	return $result->fetchAll(PDO::FETCH_ASSOC);
}
