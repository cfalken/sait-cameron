<?php

  require 'ideasDb.php';

  switch( $_REQUEST['action'] )
  {
    case 'get':
      $ideas = getIdeas();
      echo json_encode( $ideas );
      break;
    case 'save':
      saveIdea($_REQUEST['idea']);
      echo json_encode(['status' => 'saved']);
      break;
    case 'delete':
      deleteIdea($_REQUEST['id']);
      echo "Deleted";
      break;
    case 'setup':
      setupDatabase();

  }
