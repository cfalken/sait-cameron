<?php include 'head.php' ?>

<form method='POST' class="container">

  <div class="form-group">
    <label for="tweet">Tweet</label>
    <input name="tweet" class="form-control must-be-dave" type="text" />
  </div>

  <div class="form-group">
    <label for="tweet2">Name</label>
    <input id="name" name="tweet2" class="form-control must-be-dave" type="text" />
  </div>

  <input name="submit" value="Submit" type="submit" />

</form>

<ul id="tweets"></ul>

<button id="delete-something">Delete</button>

<button id="load-ajax">Load Ajax</button>
