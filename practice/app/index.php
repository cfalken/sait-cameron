<?php include 'head.php' ?>

<form method='POST' class="container">

  <div class="form-group">
    <label for="tweet">Tweet</label>
    <input id="tweet" name="tweet" class="form-control must-be-dave" type="text" />
  </div>

  <div class="form-group">
    <input type="date" name='date' />
  </div>

  <input name="submit" value="Submit" type="submit" />

</form>

<button id="trigger-dialog">Trigger Dialog</button>

<div id="dialog" title="Basic dialog" style="display:none;">
  <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>

<ul id="tweets"></ul>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">SAIT WBDV</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Goodbye</button>
        <button type="button" class="btn btn-primary">Way To Go</button>
      </div>
    </div>
  </div>
</div>
