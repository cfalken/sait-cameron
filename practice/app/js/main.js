function storeSomething(key, value)
{
  localStorage[key] = JSON.stringify(value);
}

function retrieveSomething(key)
{
  var stringValue = localStorage[key];
  if( stringValue == undefined ) {
    return undefined;
  }
  return JSON.parse(stringValue);
}

// not necessary, not adding any new functionality
function deleteSomething(key) {
  localStorage.removeItem(key);
}

function runTimer() {

  var count = 0;

  function doSomething() {
    console.log(new Date());
    timer = setTimeout(doSomething, 2000);
  }

    // setInterval(function(){
  //   count++;
  //   console.log('interval '+count, new Date());
  // }, 1500);

  timer = setTimeout(doSomething, 2000);

}

function stopTimer() {
  clearTimeout(timer);
}

function validateForm() {
  var $form = $('form');

  $form.submit(function(event){
    var validDave = validateDave($form);

    if( !validDave ) {
      alert("you must be Dave");
      event.preventDefault();
    } else {
      alert("hi Dave");
      storeSomething('message', 'undefined');
    }
  });
}

function validateDave($form) {
  var $inputs = $form.find('.must-be-dave');

  var allGood = true;

  $inputs.each(function(i, element){
    var $element = $(element);
    // if( $input.val() == 'Dave' ) {
    if( !$element.val().match(/Dave/) ) {
      $element.after('<span class="help-block">You must be Dave</span>');
      allGood = false;
    }
  });

  return allGood;

}
