<?php

  require 'tweetsDb.php';

  $action = $_REQUEST['action'];

  switch($action)
  {
    case 'save':
      $id = saveTweet($_REQUEST['tweet']);
      echo json_encode(['id'=>$id]);
      break;
    case 'delete':
      deleteTweet($_REQUEST['id']);
      break;
    case 'get':
      $tweets = getTweets();
      echo json_encode( $tweets );
      break;
  }

?>
