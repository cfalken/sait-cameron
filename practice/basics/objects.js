var myObject = {
  prop: "value",
  name: "Cameron",
  url: "here/index.html",
  doSomething: function() {
    console.log('Doing Something');
  }
};

myObject.name = "New Name";
myObject['name'] = "Better Name";

// myObject.doSomething();
// console.log(myObject.name);

function Person(name) {
  this.name = name;
  this.url = 'haha.html';
  this.doSomething = function() {
    console.log('Doing Something');
  }
}

var person = new Person('Cameron');
person.name = "Bob";
person['name'] = "Jack";
person.doSomething();
