$(document).ready(function(){

  validateForm();
  displayMessage();
  deleteMessage();
  runTimer();

});

function displayMessage() {
  var message = retrieveSomething('message');
  if( message != undefined ) {
    $('body').append('<h3>'+ message +'</h3>');
  }
}

function deleteMessage() {
  $('#delete-something').click(function(){
    localStorage.removeItem('message');
    location.reload();
  });
}
