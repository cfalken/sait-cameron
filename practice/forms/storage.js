function storeSomething(key, value)
{
  localStorage[key] = JSON.stringify(value);
}

function retrieveSomething(key)
{
  var stringValue = localStorage[key];
  if( stringValue == undefined ) {
    return undefined;
  }
  return JSON.parse(stringValue);
}

// not necessary, not adding any new functionality
function deleteSomething(key) {
  localStorage.removeItem(key);
}
