function runTimer() {

  var count = 0;

  function doSomething() {
    console.log(new Date());
    timer = setTimeout(doSomething, 2000);
  }

    // setInterval(function(){
  //   count++;
  //   console.log('interval '+count, new Date());
  // }, 1500);

  timer = setTimeout(doSomething, 2000);

}

function stopTimer() {
  clearTimeout(timer);
}
