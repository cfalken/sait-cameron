<?php include 'head.php' ?>
<h1>jQuery</h1>
<ul class="master">
  <li id="l1">
    <h2>List 1</h2>
    <ul>
      <li class="apple">Item 1A</li>
      <li class="pear">Item 1B</li>
      <li class="fruit-orange">Item 1C</li>
    </ul>
  </li>
  <li id="l2">
    <h2>List 2</h2>
    <ul>
      <li class="pear">Item 2A</li>
      <li class="apple">Item 2B</li>
      <li class="fruit-grape">Item 2C</li>
    </ul>
  </li>
  <li id="l3">
    <h2>List 3</h2>
    <ul>
      <li class="apple">Item 3A</li>
      <li class="fruit-kiwi">Item 3B</li>
      <li id="target" class="pear">Item 3C</li>
    </ul>
  </li>
</ul>

<form>
  <div class="form-group">
    <p class="error">This is required</p>
    <input type="text" name="tweet" />
  </div>
</form>

<article id="my-article">
  <p>My brilliant prose.</p>
  <a href="" class="changeme">Change Me</a>
</article>

<button id="trigger">Trigger</button>
